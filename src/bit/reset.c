
unsigned int reset(unsigned int a, int b)
{
	b--; 		//because index starts from zero
	a &=~(1<<b);
	return a;
}
