#include<stdarg.h>
int vsum(int n, ...)
{
	int sum=0,i=0;
	va_list args;

	va_start(args,n);
	for(i=0;i<n;i++)
	{
		sum+=va_arg(args,int);
	}
	va_end(args);
	return sum;
}
