
int isPalindrome(int a)
{
	int n=a, r=0,sum=0;
	while(n!=0)
	{
		r = n%10;
		n=n/10;
		sum = sum*10+r;
	}
	if(sum == a)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
