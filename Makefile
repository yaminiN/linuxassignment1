TARGET=all.out
STROBJ=src/str/mystrlen.o src/str/mystrcmp.o src/str/mystrcat.o src/str/mystrcpy.o
UTILOBJ=src/util/factorial.o src/util/isPrime.o src/util/isPalindrome.o src/util/vsum.o
BITOBJ=src/bit/set.o src/bit/reset.o src/bit/flip.o src/bit/query1.o
TOPDIR =${pwd}
CFLAGS=-I${TOPDIR}/include

all:${TARGET}
${TARGET}:testfun.o libmystring.a libbitmask.a libmyutils.a
	gcc -L. $^ -o $@ -lmyutils -lmystring -lbitmask
libmystring.a:${STROBJ}
	ar rc $@ $^
libbitmask.a:${BITOBJ}
	ar rc $@ $^
libmyutils.a:${UTILOBJ}
	ar rc $@ $^
%.o:src/%.c include/mystring.h
	gcc $< -c -g
testfun.o:testfun.c include/mystring.h include/myutils.h include/bitmask.h
	gcc $< -c -g
clean:
	rm -rf *.o src/bit/*.o src/str/*.o src/util/*.o  *.a

	

